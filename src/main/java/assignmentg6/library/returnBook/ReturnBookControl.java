package library.returnBook;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

	private ReturnBookUI returnBookUI;

	private enum ControlState {
		INITIALISED, READY, INSPECTING
	};

	private ControlState controlState;

	private Library library;
	private Loan currentLoan;

	public ReturnBookControl() {
		this.library = Library.getInstance();
		controlState = ControlState.INITIALISED;
	}

	public void setUi(ReturnBookUI returnBookUI) {
		if (!controlState.equals(ControlState.INITIALISED)) {
			throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
		}

		this.returnBookUI = returnBookUI;
		returnBookUI.setState(ReturnBookUI.UiState.READY);
		controlState = ControlState.READY;
	}

	public void bookScanned(int bookId) {
		if (!controlState.equals(ControlState.READY)) {
			throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
		}

		Book currentBook = library.getBook(bookId);

		if (currentBook == null) {
			returnBookUI.display("Invalid Book Id");
			return;
		}
		if (!currentBook.isOnLoan()) {
			returnBookUI.display("Book has not been borrowed");
			return;
		}
		currentLoan = library.getLoanByBookId(bookId);
		double overDueFine = 0.0;
		if (currentLoan.idOverDue()) {
			overDueFine = library.calculateOverDueFine(currentLoan);
		}

		returnBookUI.display("Inspecting");
		returnBookUI.display(currentBook.toString());
		returnBookUI.display(currentLoan.toString());

		if (currentLoan.idOverDue()) {
			returnBookUI.display(String.format("\nOverdue fine : $%.2f", overDueFine));
		}

		returnBookUI.setState(ReturnBookUI.UiState.INSPECTING);
		controlState = ControlState.INSPECTING;
	}

	public void scanningComplete() {
		if (!controlState.equals(ControlState.READY)) {
			throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
		}

		returnBookUI.setState(ReturnBookUI.UiState.COMPLETED);
	}

	public void dischargeLoan(boolean isDamaged) {
		if (!controlState.equals(ControlState.INSPECTING)) {
			throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
		}

		library.dischargeLoan(currentLoan, isDamaged);
		currentLoan = null;
		returnBookUI.setState(ReturnBookUI.UiState.READY);
		controlState = ControlState.READY;
	}

}
