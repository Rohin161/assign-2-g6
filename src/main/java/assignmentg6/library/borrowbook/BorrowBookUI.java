package library.borrowbook;
import java.util.Scanner;


public class BorrowBookUI {
	
	public static enum UserInterfaceState { INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED };

	private BorrowBookControl control;
	private Scanner input;
	private UserInterfaceState state;

	
	public BorrowBookUI(BorrowBookControl control) {
		this.control = control;
		input = new Scanner(System.in);
		state = UserInterfaceState.INITIALISED;
		control.setUi(this);
	}

	
	private String getInput(String prompt) {
		System.out.print(prompt);
		return input.nextLine();
	}	
		
		
	private void viewOutput(Object object) {
		System.out.println(object);
	}
	
			
	public void setState(UserInterfaceState State) {
		this.state = State;
	}

	
	public void run() {
		viewOutput("Borrow Book Use Case UI\n");
		
		while (true) {
			
			switch (state) {			
			
			case CANCELLED:
				viewOutput("Borrowing Cancelled");
				return;

				
			case READY:
				String memberString = getInput("Swipe member card (press <enter> to cancel): ");
				if (memberString.length() == 0) {
					control.cancel();
					break;
				}
				try {
					int memberId = Integer.valueOf(memberString).intValue();
					control.swiped(memberId);
				}
				catch (NumberFormatException e) {
					viewOutput("Invalid Member Id");
				}
				break;

				
			case RESTRICTED:
				getInput("Press <any key> to cancel");
				control.cancel();
				break;
			
				
			case SCANNING:
				String bookStringInput = getInput("Scan Book (<enter> completes): ");
				if (bookStringInput.length() == 0) {
					control.complete();
					break;
				}
				try {
					int bookId = Integer.valueOf(bookStringInput).intValue();
					control.Scanned(bookId);
					
				} catch (NumberFormatException e) {
					viewOutput("Invalid Book Id");
				} 
				break;
					
				
			case FINALISING:
				String answer = getInput("Commit loans? (Y/N): ");
				if (answer.toUpperCase().equals("N")) {
					control.cancel();
					
				} else {
					control.commitLoans();
					getInput("Press <any key> to complete ");
				}
				break;
				
				
			case COMPLETED:
				viewOutput("Borrowing Completed");
				return;
	
				
			default:
				viewOutput("Unhandled state");
				throw new RuntimeException("BorrowBookUI : unhandled state :" + state);			
			}
		}		
	}


	public void display(Object object) {
		viewOutput(object);		
	}


}
