package library.borrowbook;

import java.util.ArrayList;
import java.util.List;

import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;

public class BorrowBookControl {

    private BorrowBookUI userInterface;

    private Library library;
    private Member member;

    private enum ControlState {
        INITIALISED, READY, RESTRICTED, SCANNING, IDENTIFIED, FINALISING, COMPLETED, CANCELLED
    };
    private ControlState state;

    private List<Book> pendingList;
    private List<Loan> completedList;
    private Book book;

    public BorrowBookControl() {
        this.library = Library.getInstance();
        state = ControlState.INITIALISED;
    }

    public void setUi(BorrowBookUI ui) {
        if (!state.equals(ControlState.INITIALISED)) {
            throw new RuntimeException("BorrowBookControl: cannot call setUI except in INITIALISED state");
        }

        this.userInterface = ui;
        ui.setState(BorrowBookUI.UserInterfaceState.READY);
        state = ControlState.READY;
    }

    public void swiped(int numberId) {
        if (!state.equals(ControlState.READY)) {
            throw new RuntimeException("BorrowBookControl: cannot call cardSwiped except in READY state");
        }

        member = library.getMember(numberId);
        if (member == null) {
            userInterface.display("Invalid memberId");
            return;
        }
        if (library.canMemberBorrow(member)) {
            pendingList = new ArrayList<>();
            userInterface.setState(BorrowBookUI.UserInterfaceState.SCANNING);
            state = ControlState.SCANNING;
        } else {
            userInterface.display("Member cannot borrow at this time");
            userInterface.setState(BorrowBookUI.UserInterfaceState.RESTRICTED);
        }
    }

    public void Scanned(int bookId) {
        book = null;
        if (!state.equals(ControlState.SCANNING)) {
            throw new RuntimeException("BorrowBookControl: cannot call bookScanned except in SCANNING state");
        }

        book = library.getBook(bookId);
        if (book == null) {
            userInterface.display("Invalid bookId");
            return;
        }
        if (!book.isAvailable()) {
            userInterface.display("Book cannot be borrowed");
            return;
        }
        pendingList.add(book);
        for (Book book : pendingList) {
            userInterface.display(book.toString());
        }

        if (library.getNumberOfLoansRemainingForMember(member) - pendingList.size() == 0) {
            userInterface.display("Loan limit reached");
            complete();
        }
    }

    public void complete() {
        if (pendingList.size() == 0) {
            cancel();
        } else {
            userInterface.display("\nFinal Borrowing List");
            for (Book book : pendingList) {
                userInterface.display(book.toString());
            }

            completedList = new ArrayList<Loan>();
            userInterface.setState(BorrowBookUI.UserInterfaceState.FINALISING);
            state = ControlState.FINALISING;
        }
    }

    public void commitLoans() {
        if (!state.equals(ControlState.FINALISING)) {
            throw new RuntimeException("BorrowBookControl: cannot call commitLoans except in FINALISING state");
        }

        for (Book book : pendingList) {
            Loan loan = library.issueLoan(book, member);
            completedList.add(loan);
        }
        userInterface.display("Completed Loan Slip");
        for (Loan loan : completedList) {
            userInterface.display(loan.toString());
        }

        userInterface.setState(BorrowBookUI.UserInterfaceState.COMPLETED);
        state = ControlState.COMPLETED;
    }

    public void cancel() {
        userInterface.setState(BorrowBookUI.UserInterfaceState.CANCELLED);
        state = ControlState.CANCELLED;
    }

}
